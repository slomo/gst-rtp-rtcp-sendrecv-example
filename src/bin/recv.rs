use gst::{glib, prelude::*};

use anyhow::Error;

use futures::prelude::*;

const RTPBIN2: bool = true;

struct Pipeline(gst::Pipeline);

impl std::ops::Deref for Pipeline {
    type Target = gst::Pipeline;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Drop for Pipeline {
    fn drop(&mut self) {
        let _ = self.0.set_state(gst::State::Null);
    }
}

fn main() -> Result<(), Error> {
    let ctx = glib::MainContext::default();

    gst::init()?;

    let pipeline = Pipeline(gst::Pipeline::new());

    let rtpbin = gst::ElementFactory::make(if RTPBIN2 { "rtpbin2" } else { "rtpbin" })
        .property_from_str("rtp-profile", "avpf")
        .property("reduced-size-rtcp", true)
        .build()?;

    let rtp_src = gst::ElementFactory::make("udpsrc")
        .property("address", "127.0.0.1")
        .property("port", 5400i32)
        .property(
            "caps",
            gst::Caps::builder("application/x-rtp")
                .field("media", "video")
                .field("clock-rate", 90_000i32)
                .field("encoding-name", "VP8")
                .field("payload", 96i32)
                .field("rtcp-fb-nack-pli", true)
                .field("rtcp-fb-ccm-fir", true)
                .build(),
        )
        .build()?;

    let rtcp_src = gst::ElementFactory::make("udpsrc")
        .property("address", "127.0.0.1")
        .property("port", 5401i32)
        .build()?;

    let rtcp_sink = gst::ElementFactory::make("udpsink")
        .property("host", "127.0.0.1")
        .property("port", 5403i32)
        .property("sync", false)
        .property("async", false)
        .build()?;

    let depay = gst::ElementFactory::make("rtpvp8depay").build()?;
    let dec = gst::ElementFactory::make("vp8dec").build()?;
    let q1 = gst::ElementFactory::make("queue").build()?;
    let conv = gst::ElementFactory::make("videoconvert").build()?;
    let sink = gst::ElementFactory::make("autovideosink").build()?;

    pipeline
        .add_many([
            &rtpbin, &rtp_src, &rtcp_src, &rtcp_sink, &depay, &dec, &q1, &conv, &sink,
        ])
        .unwrap();

    gst::Element::link_many([&depay, &dec, &q1, &conv, &sink]).unwrap();

    rtp_src
        .link_pads(
            Some("src"),
            &rtpbin,
            Some(if RTPBIN2 {
                "rtp_recv_sink_0"
            } else {
                "recv_rtp_sink_0"
            }),
        )
        .unwrap();
    rtcp_src
        .link_pads(
            Some("src"),
            &rtpbin,
            Some(if RTPBIN2 {
                "rtcp_recv_sink_0"
            } else {
                "recv_rtcp_sink_0"
            }),
        )
        .unwrap();
    rtpbin
        .link_pads(
            Some(if RTPBIN2 {
                "rtcp_send_src_0"
            } else {
                "send_rtcp_src_0"
            }),
            &rtcp_sink,
            Some("sink"),
        )
        .unwrap();

    rtpbin.connect_pad_added({
        let depay = depay.clone();
        move |_rtpbin, srcpad| {
            println!("New RTP source pad {}", srcpad.name());

            let sinkpad = depay.static_pad("sink").unwrap();

            if let Some(peer) = sinkpad.peer() {
                println!("Unlinking old stream");
                let _ = peer.unlink(&sinkpad);
            }

            srcpad.link(&sinkpad).unwrap();
        }
    });

    let bus = pipeline
        .bus()
        .expect("Pipeline without bus. Shouldn't happen!");

    pipeline
        .set_state(gst::State::Playing)
        .expect("Unable to set the pipeline to the `Playing` state");

    glib::timeout_add_seconds(2, {
        let depay = depay.clone();
        move || {
            println!("sending fku");
            depay.send_event(gst_video::UpstreamForceKeyUnitEvent::builder().build());
            glib::ControlFlow::Continue
        }
    });

    ctx.block_on(async {
        let mut stream = bus.stream();

        while let Some(msg) = stream.next().await {
            use gst::MessageView;

            match msg.view() {
                MessageView::Eos(..) => {
                    println!("EOS");
                    break;
                }
                MessageView::Error(err) => {
                    println!(
                        "Error from {:?}: {} ({:?})",
                        err.src().map(|s| s.path_string()),
                        err.error(),
                        err.debug()
                    );

                    break;
                }
                _ => (),
            }
        }
    });

    pipeline
        .set_state(gst::State::Null)
        .expect("Unable to set the pipeline to the `Null` state");

    Ok(())
}
