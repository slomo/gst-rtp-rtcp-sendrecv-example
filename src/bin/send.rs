use gst::{glib, prelude::*};

use anyhow::Error;

use futures::prelude::*;

const RTPBIN2: bool = true;

struct Pipeline(gst::Pipeline);

impl std::ops::Deref for Pipeline {
    type Target = gst::Pipeline;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Drop for Pipeline {
    fn drop(&mut self) {
        let _ = self.0.set_state(gst::State::Null);
    }
}

fn main() -> Result<(), Error> {
    let ctx = glib::MainContext::default();

    gst::init()?;

    let pipeline = Pipeline(gst::Pipeline::new());

    let rtpbin = gst::ElementFactory::make(if RTPBIN2 { "rtpbin2" } else { "rtpbin" })
        .property_from_str("rtp-profile", "avpf")
        .build()?;

    let src = gst::ElementFactory::make("videotestsrc")
        .property_from_str("pattern", "ball")
        .build()?;
    let capsfilter = gst::ElementFactory::make("capsfilter")
        .property(
            "caps",
            gst_video::VideoCapsBuilder::new()
                .format(gst_video::VideoFormat::I420)
                .width(1280)
                .height(720)
                .framerate(gst::Fraction::new(30, 1))
                .build(),
        )
        .build()?;
    let q1 = gst::ElementFactory::make("queue").build()?;
    let enc = gst::ElementFactory::make("vp8enc")
        .property("keyframe-max-dist", 30i32)
        .property("threads", 12i32)
        .property("cpu-used", -16i32)
        .property("deadline", 1i64)
        .property_from_str("error-resilient", "default")
        .build()?;
    let q2 = gst::ElementFactory::make("queue").build()?;
    let pay = gst::ElementFactory::make("rtpvp8pay")
        .property_from_str("picture-id-mode", "7-bit")
        .build()?;

    let rtcp_src = gst::ElementFactory::make("udpsrc")
        .property("address", "127.0.0.1")
        .property("port", 5403i32)
        .build()?;

    let rtp_sink = gst::ElementFactory::make("udpsink")
        .property("host", "127.0.0.1")
        .property("port", 5400i32)
        .property("sync", true)
        .build()?;

    let rtcp_sink = gst::ElementFactory::make("udpsink")
        .property("host", "127.0.0.1")
        .property("port", 5401i32)
        .property("sync", false)
        .property("async", false)
        .build()?;

    pipeline
        .add_many([
            &rtpbin,
            &src,
            &capsfilter,
            &q1,
            &enc,
            &q2,
            &pay,
            &rtcp_src,
            &rtp_sink,
            &rtcp_sink,
        ])
        .unwrap();

    gst::Element::link_many([&src, &capsfilter, &q1, &enc, &q2, &pay]).unwrap();

    pay.link_pads(
        Some("src"),
        &rtpbin,
        Some(if RTPBIN2 {
            "rtp_send_sink_0"
        } else {
            "send_rtp_sink_0"
        }),
    )
    .unwrap();
    rtcp_src
        .link_pads(
            Some("src"),
            &rtpbin,
            Some(if RTPBIN2 {
                "rtcp_recv_sink_0"
            } else {
                "recv_rtcp_sink_0"
            }),
        )
        .unwrap();
    rtpbin
        .link_pads(
            Some(if RTPBIN2 {
                "rtp_send_src_0"
            } else {
                "send_rtp_src_0"
            }),
            &rtp_sink,
            Some("sink"),
        )
        .unwrap();
    rtpbin
        .link_pads(
            Some(if RTPBIN2 {
                "rtcp_send_src_0"
            } else {
                "send_rtcp_src_0"
            }),
            &rtcp_sink,
            Some("sink"),
        )
        .unwrap();

    let bus = pipeline
        .bus()
        .expect("Pipeline without bus. Shouldn't happen!");

    pipeline
        .set_state(gst::State::Playing)
        .expect("Unable to set the pipeline to the `Playing` state");

    ctx.block_on(async {
        let mut stream = bus.stream();

        while let Some(msg) = stream.next().await {
            use gst::MessageView;

            match msg.view() {
                MessageView::Eos(..) => {
                    println!("EOS");
                    break;
                }
                MessageView::Error(err) => {
                    println!(
                        "Error from {:?}: {} ({:?})",
                        err.src().map(|s| s.path_string()),
                        err.error(),
                        err.debug()
                    );

                    break;
                }
                _ => (),
            }
        }
    });

    pipeline
        .set_state(gst::State::Null)
        .expect("Unable to set the pipeline to the `Null` state");

    Ok(())
}
